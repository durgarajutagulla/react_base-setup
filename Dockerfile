FROM node:10-alpine

WORKDIR /react-ui

COPY . .

RUN  npm i

RUN npm run build

FROM nginx:alpine


RUN rm -rf /usr/share/n

COPY  build /usr/share/nginx/html

# EXPOSE 8080 80
